import logging

import streamlit as st
from dotenv import load_dotenv
from prompt import ask_question

import os

# Charge les variables d'environnement
load_dotenv()

# Configure le logger
logging.basicConfig(format='%(levelname)s: %(message)s',
                    encoding='utf-8', level=logging.ERROR)

# Configuration de l'application
st.set_page_config(
    layout="wide",
    page_title="Lab IA 🧪",
    initial_sidebar_state="expanded",)

def cs_sidebar(screen_id):
    st.sidebar.header('🦖 Codelab IA')
    st.sidebar.markdown('''<hr>''', unsafe_allow_html=True)

    st.sidebar.markdown('## Liens utiles')
    st.sidebar.markdown('''
    <small>Consignes : [Lab IA 🧪](https://pages.lab-ia.cloud-sp.eu/)</small><br>
    <small>Documentation : [Langchain 🦜](https://python.langchain.com/v0.2/docs/introduction/)</small>
        ''', unsafe_allow_html=True)
    st.sidebar.markdown('''<hr>''', unsafe_allow_html=True)

    st.sidebar.markdown('## Paramètres')
    temperature = st.sidebar.slider('Température du modèle : 0 : Précis / 1 : Créatif', 0.0, 1.0, 0.5, 0.1, key=f'slider_temperature_{screen_id}') # min, max, default, step
    os.environ["TEMPERATURE"] = str(temperature)

    if st.sidebar.button('Nouvelle conversation', key=f'nc_{screen_id}'):
        st.session_state.clear()

cs_sidebar(0)

st.markdown("""
🔬 Lab IA 🔬
============

Bienvenue dans votre interface pour interagir avec le modèle de langage au travers du RAG et des Agents.

"""
)

# Rappel : streamlit est lancé sur la webapp spawné avec l'environnement de dev

# Code à compléter : Ajouter un champ de saisie pour poser la question via l'api configurée précédemment, puis afficher la réponse
# Référence(s) documentaire(s) : 
# - https://docs.streamlit.io/develop/api-reference/widgets/st.text_input
# - https://docs.streamlit.io/develop/api-reference/write-magic/st.write
# Des exemples sont donnés dans la documentation de Streamlit
