"""Constantes applicatives"""

import os

from dotenv import load_dotenv

# Charge les fichiers .env
load_dotenv()

DATABASE_DIR = os.environ.get("DATABASE_DIR", "./database")
"""Répertoire de la base de données"""
DOCUMENTS_DIR = os.environ.get("DOCUMENTS_DIR", "./documents")
"""Répertoire des documents à charger"""
EMBEDDINGS_MODEL = os.environ.get("EMBEDDINGS_MODEL", "text-embedding-ada-002")
"""Modèle d'embeddings à utiliser"""
MODEL_NAME = os.environ.get("MODEL_NAME", "gpt-35-turbo-16k")
"""Nom du modèle à utiliser"""

# Hyperparamètres
TEMPERATURE = float(os.environ.get("TEMPERATURE", 0.5))
"""Température du modèle"""

OVH_AI_ENDPOINTS_ACCESS_TOKEN = os.environ.get("OVH_AI_ENDPOINTS_ACCESS_TOKEN", None)
"""Clé d'API OVH AI Endpoints"""
