"""
Ingestion des documents
=======================

Etapes à réaliser:

1. Charger les documents PDF avec PyPDF
2. Splitter les textes
3. Calculer les embeddings
4. Les stocker dans une base vecteur
"""

import logging
import os

from langchain_community.document_loaders import PyPDFDirectoryLoader
from util_API_OVH.embedding import OVHcloudAIEEmbeddings
from langchain_chroma import Chroma

import constants

# Configure le logger
logging.basicConfig(format='%(levelname)s: %(message)s',
                    encoding='utf-8', level=logging.INFO)

# Code à compléter : DB à initialiser, avec persistence local dans le dossier : constants.DATABASE_DIR
# Référence(s) documentaire(s) : 
# - https://python.langchain.com/docs/integrations/vectorstores/chroma/
# avec utilisation de embedding_function=OVHcloudAIEEmbeddings(api_key=os.environ.get("OVH_AI_ENDPOINTS_ACCESS_TOKEN", None))


def ingest(directory):
    """Fonction principale d'ingestion des documents"""
    
    # Code à compléter : Load & split
    # Référence(s) documentaire(s) :
    # - https://python.langchain.com/v0.1/docs/modules/data_connection/vectorstores/
    documents = []
    logging.info("Chargement des fichiers depuis le répertoire %s", directory)

    # Code à compléter : Embed & Store
    # Référence(s) documentaire(s) : 
    # - https://python.langchain.com/docs/integrations/vectorstores/chroma/
    # Il suffit d'ajouter dans la base de données les documents précédemment chargés et passé en paramètre
    logging.info("Nombre de documents chargés: %i", len(documents))


if __name__ == "__main__":
    ingest(directory=constants.DOCUMENTS_DIR)
