import streamlit as st
from Home import cs_sidebar
from prompt_rag import ask_question

cs_sidebar(1)


# Titre de l'application
st.title("RAG 💬")
st.divider()
st.markdown("""
Cet assistant permet d'interroger un corpus documentaire pour enrichir la connaissance du chatbot.
            
* Posez vos questions en **français** pour interroger les documents en français
* Posez vos questions en **anglais** pour interroger les documents en anglais
""")

if "messages" not in st.session_state.keys():
    st.session_state.messages = [
        {"role": "assistant",
         "content": "Je suis prêt à répondre à vos questions !"}]

# Display chat messages
for message in st.session_state.messages:
    with st.chat_message(message["role"], avatar="images/" + message['role'] + ".png"):
        if message.get("sources"):
            st.markdown("**Ma réponse :**")
            st.write(message["content"])

            unique_sources = set()
            with st.expander("📚 Documents utilisés", expanded=False):
                for source in message["sources"]:
                    unique_sources.add((source.metadata['source'], source.metadata['page']))

                for source in unique_sources:
                    st.markdown(f"* Doc: {source[0]}, Page: {source[1]}")
        else:
            st.write(message["content"])

# Display the prompt
if prompt := st.chat_input():
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user", avatar="images/user.png"):
        st.write(prompt)

# Generate a new response if last message is not from assistant
if st.session_state.messages[-1]["role"] != "assistant":
    with st.chat_message("assistant", avatar="images/assistant.png"):
        with st.spinner("Réponse en cours de génération..."):
            response, sources = ask_question(prompt)
            st.markdown("**Ma réponse :**")
            st.write(response)
            
            unique_sources = set()
            with st.expander("📚 Documents utilisés", expanded=True):
                for source in sources:
                    unique_sources.add((source.metadata['source'], source.metadata['page']))

                for source in unique_sources:
                    st.markdown(f"* Doc: {source[0]}, Page: {source[1]}")
    message = {"role": "assistant", "content": response, "sources": sources}
    st.session_state.messages.append(message)
