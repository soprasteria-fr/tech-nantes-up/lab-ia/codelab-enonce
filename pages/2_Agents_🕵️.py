import streamlit as st
from Home import cs_sidebar
from prompt_agent import ask_question

cs_sidebar(2)


# Titre de l'application
st.title("Agent 💬")
st.divider()

if "messages_agent" not in st.session_state.keys():
    st.session_state.messages_agent = [
        {"role": "assistant",
         "content": "Je suis prêt à répondre à vos questions !"}]

# Display chat messages_agent
for message in st.session_state.messages_agent:
    with st.chat_message(message["role"], avatar="images/" + message['role'] + ".png"):
        if message.get("steps"):
            st.markdown("**Ma réponse :**")
            st.write(message["content"])

            with st.expander("Etapes réflexion", expanded=False):
                for step in message["steps"]:
                    st.write(step)
        else:
            st.write(message["content"])

# Display the prompt
if prompt := st.chat_input():
    st.session_state.messages_agent.append({"role": "user", "content": prompt})
    with st.chat_message("user", avatar="images/user.png"):
        st.write(prompt)

# Generate a new response if last message is not from assistant
if st.session_state.messages_agent[-1]["role"] != "assistant":
    with st.chat_message("assistant", avatar="images/assistant.png"):
        with st.spinner("Réponse en cours de génération..."):
            response, steps = ask_question(prompt)
            st.markdown("**Ma réponse :**")
            st.write(response)

            with st.expander("Etapes réflexion", expanded=True):
                for step in steps:
                    st.write(step)

    message = {"role": "assistant", "content": response, "steps": steps}
    st.session_state.messages_agent.append(message)
