"""Prompt classique"""

import logging

from langchain_openai import ChatOpenAI as Chat
from langchain.prompts.chat import (ChatPromptTemplate,
                                    HumanMessagePromptTemplate,
                                    SystemMessagePromptTemplate)

import constants

# Configure le logger
logging.basicConfig(format='%(levelname)s: %(message)s',
                    encoding='utf-8', level=logging.ERROR)

# template du prompt Système
SYSTEM_PROMPT = """
Tu es un expert <à compléter avec les consignes pour définir son comportement>.
Si tu ne connais pas la réponse, dites simplement que tu ne sais pas. N'essayez pas d'inventer une réponse.
===========
"""

CHAT = Chat(model="Mixtral-8x22B-Instruct-v0.1",
                        api_key=constants.OVH_AI_ENDPOINTS_ACCESS_TOKEN,
                        base_url='https://mixtral-8x22b-instruct-v01.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1', 
                        max_tokens=1500)

def ask_question(question):
    """Poser une question au modèle."""

    # Constitue la séquence de chat avec le conditionnement du bot et la question
    # de l'utilisateur
    system_message_prompt = SystemMessagePromptTemplate.from_template(
        SYSTEM_PROMPT)
    human_message_prompt = HumanMessagePromptTemplate.from_template(
        "QUESTION: {question}")
    chat_prompt = ChatPromptTemplate.from_messages(
        [system_message_prompt, human_message_prompt]
    )
    messages = chat_prompt.format_prompt(
        question=question
    ).to_messages()

    # Code à compléter : pour appeler le modèle et générer la réponse ;
    # Référence(s) documentaire(s) : 
    # - https://python.langchain.com/docs/integrations/chat/openai/
    # L'instanciation est déjà réalisée dans la variable CHAT, il faut juste réaliser l'invocation 

    response = ""

    return response


if __name__ == "__main__":
    print("""
Posez simplement votre question:
""")
    print("----------------------------------------------------")
    print("💬 Votre question: ")

    answer = ask_question(input())

    print("----------------------------------------------------")
    print("🤖 Réponse du LLM: ")
    print(answer)
    print("----------------------------------------------------")
