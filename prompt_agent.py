"""Prompt avec agents."""

import logging

from langchain import hub
from langchain.agents import AgentExecutor, create_tool_calling_agent
from langchain.chains.llm_math.base import LLMMathChain
from langchain.tools import Tool
from langchain_openai import AzureChatOpenAI as Chat

import constants

# Configure le logger
logging.basicConfig(
    format="%(levelname)s: %(message)s", encoding="utf-8", level=logging.ERROR
)

# Instanciation du chat
_CHAT = Chat(model_name="gpt-4o", temperature=constants.TEMPERATURE)

# Déclaration des outils
_TOOLS = [
    Tool.from_function(
        func=LLMMathChain.from_llm(llm=_CHAT, verbose=True).run,
        name="Calculator",
        description="Useful when you need to answer questions about math",
        handle_tool_error=True,
    ),
]

# Initialisation de l'agent
prompt = hub.pull("hwchase17/openai-tools-agent")
agent = create_tool_calling_agent(_CHAT, _TOOLS, prompt)
agent_executor = AgentExecutor(agent=agent, tools=_TOOLS, verbose=True)


def ask_question(question):
    """Poser une question au modèle avec l'agent et ses outils."""
    response = None
    used_tools = []

    # Lance l'évaluation du prompt
    for chunk in agent_executor.stream({"input": question}):
        # Enregistrement des actions du LLM
        if "actions" in chunk:
            for action in chunk["actions"]:
                used_tools.append(action.tool)
                response = action.tool_input
        if "output" in chunk:
            response = chunk["output"]
    return response, used_tools


if __name__ == "__main__":
    print("""\n🕵️  Agent 🕵️\n""")
    print("💬 Votre question: ")
    answer, tools_list = ask_question(input())
    print("🕵️  Réponse de l'agent: ")
    print(answer)
    print("\n🔨 Outils utilisées: ")
    for tool in tools_list:
        print(f"    ◼ {tool}")
