"""Prompt pour le RAG."""

import logging
import os
from unittest import result
import constants

from langchain_openai import ChatOpenAI as Chat
from util_API_OVH.embedding import OVHcloudAIEEmbeddings
from langchain_chroma import Chroma
from langchain.prompts.chat import (ChatPromptTemplate,
                                    HumanMessagePromptTemplate,
                                    SystemMessagePromptTemplate)

# Configure le logger
logging.basicConfig(format='%(levelname)s: %(message)s',
                    encoding='utf-8', level=logging.ERROR)

# template du prompt Système
SYSTEM_PROMPT = """
Tu es un expert <à compléter avec les consignes pour définir son comportement>.
Si tu ne connais pas la réponse, dites simplement que tu ne sais pas. N'essayez pas d'inventer une réponse.
===========
CONTEXTE:
{context}
===========
"""

# Code à compléter : Initialisation chat
# Code à compléter : Initialisation base de données

def ask_question(question):
    """Poser une question au modèle."""

    # Code à compléter en reprenant le code de retrieving.py pour récupérer les sources
    results = []

    logging.debug("Sources: %s", results)

    # Constitue la séquence de chat avec le conditionnement du bot et la question
    # de l'utilisateur
    system_message_prompt = SystemMessagePromptTemplate.from_template(
        SYSTEM_PROMPT)
    human_message_prompt = HumanMessagePromptTemplate.from_template(
        "QUESTION: {question}")
    chat_prompt = ChatPromptTemplate.from_messages(
        [system_message_prompt, human_message_prompt]
    )
    messages = chat_prompt.format_prompt(
        context=results, question=question
    ).to_messages()

    # Code à compléter : Appel au modèle pour générer la réponse ; cf. documentation de ChatOpenAI
    response = ""

    return response, results


if __name__ == "__main__":
    print("""
Posez simplement votre question:
""")
    print("----------------------------------------------------")
    print("💬 Votre question: ")

    answer, sources = ask_question(input())

    print("----------------------------------------------------")
    print("🤖 Réponse de l'expert: ")
    print(answer)
    print("----------------------------------------------------")
    print("📚 Documents utilisés: ")
    unique_sources = set()
    for source in sources:
        unique_sources.add((source.metadata['source'], source.metadata['page']))

    for source in unique_sources:
        print(f"- Document: {source[0]}, Page: {source[1]}")
    print("----------------------------------------------------")
