"""Prompt pour le RAG."""

import logging
import os
import constants
from collections import defaultdict

from langchain_openai import ChatOpenAI as Chat
from util_API_OVH.embedding import OVHcloudAIEEmbeddings
from langchain_chroma import Chroma

# Configure le logger
logging.basicConfig(format='%(levelname)s: %(message)s',
                    encoding='utf-8', level=logging.DEBUG)

CHAT = Chat(model="Mixtral-8x22B-Instruct-v0.1",
                        api_key=constants.OVH_AI_ENDPOINTS_ACCESS_TOKEN,
                        base_url='https://mixtral-8x22b-instruct-v01.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1', 
                        max_tokens=1500)
#CHAT = Chat(model="Mixtral-8x7B-Instruct-v0.1", api_key=constants.OVH_AI_ENDPOINTS_ACCESS_TOKEN, base_url='https://mixtral-8x7b-instruct-v01.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1', max_tokens=1500)
#CHAT = Chat(model="Mistral-7B-Instruct-v0.2", api_key=constants.OVH_AI_ENDPOINTS_ACCESS_TOKEN, base_url='https://mistral-7b-instruct-v02.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1', max_tokens=1500)
DB = Chroma(persist_directory=constants.DATABASE_DIR,
            embedding_function=OVHcloudAIEEmbeddings(api_key=os.environ.get("OVH_AI_ENDPOINTS_ACCESS_TOKEN", None)))

def find_sources(question):
    """Lister les sources qui correspondent sémantiquements à la question."""

    # Code à compléter : retriever à appeler pour interroger la base de données
    # Référence(s) documentaire(s) :
    # - https://python.langchain.com/v0.1/docs/modules/data_connection/retrievers/vectorstore/
    # Investiguer les paramètres pour affiner la qualité de votre comparaison de vecteur :
    # - Type de recherche, seuil de similarité, etc.

    results = ""
    logging.debug("Sources: %s", results)

    return results


if __name__ == "__main__":
    print("""Posez simplement votre question:""")
    print("----------------------------------------------------")
    print("💬 Votre question: ")

    sources = find_sources(input())

    print("----------------------------------------------------")
    print("📚 Documents identifiés: ")
    unique_sources = set()
    source_content = defaultdict(list)

    for source in sources:
        unique_sources.add((source.metadata['source'], source.metadata['page']))
        source_content[source.metadata['source']].append(source.page_content)

    for source in unique_sources:
        print(f"- Document: {source[0]}, Page: {source[1]}")
        for content in source_content[source[0]]:
            print(f"Contenu de la page : {content}")
            print("====================================================")
    print("----------------------------------------------------")
