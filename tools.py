"""Déclaration d'outils customisés pour l'agent."""

import requests
from pydantic import BaseModel, Field
from langchain.tools import tool

@tool
def get_pokemon_details(pokemon_name):
    """Get pokemon details by its pokemon name.

    Args:
        pokemon_name (str): The name of the pokemon to get details for.

    Returns:
        dict: A dictionary containing the details of the pokemon."""
    url = f"https://pokeapi.co/api/v2/pokemon/{pokemon_name.lower()}"
    response = requests.get(url, timeout=15)
    if response.status_code == 200:
        data = response.json()
        return {
            "name": data["name"],
            "height": data["height"],
            "weight": data["weight"],
            "base_experience": data["base_experience"],
            "abilities": [ability["ability"]["name"] for ability in data["abilities"]],
        }
    return None
